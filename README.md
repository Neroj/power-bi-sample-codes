# Power BI Sample codes

Repository used to share Power BI Code publicly.
Feel free to use the code for your own projects.

# Contact
Joren Venema
<br>
https://www.linkedin.com/in/joren-venema-a18b73147/
<br>
JorenVenema@gmail.com